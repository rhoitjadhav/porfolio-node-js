const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

// require js
const test = require('./api/routes/test');

// morgan logger
app.use(morgan('dev'));


// body-parser
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());


// allow cross origin setup
app.use(cors());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Header', 'Content-Type,Accept,Authorization and works');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET,POST,DELETE,PUT');
        return res.status(200).json({});
    }
    next();
});



// registration of api
app.use('/test', cors(), test);



// api checking middleware
app.use(function (req, res, next) {
    res.status(404).json({
        message: 'api not found'
    });
});

// error handling middleware
app.use(function (err, req, res, next) {
    res.status(400).json({
        message: err.message
    });
});


// exports
module.exports = app;
