const http = require('http');
const app = require('./app');

const portNo = 3333;

const port = process.env.PORT || portNo;

const server = http.createServer(app);

server.listen(port, function() {
    console.log("server started on port: " + portNo)
});