const express = require('express');
const router = express.Router();


// --- ENDPOINT START ---
router.get('/', function (req, res, next) {
    return res.status(200).json({
        message: "test works"
    });
});

// --- ENDPOINT ENDS ---


module.exports = router;